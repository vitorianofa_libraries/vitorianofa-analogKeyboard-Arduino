#include "Arduino.h"
#include "TecladoPAnalog.h"

TecladoPAnalog::TecladoPAnalog(int pin)
{
	_pin=pin;
}


void TecladoPAnalog::resistores(float R0,float R1,float R2,float R3,float R4,float R5)
{
	_valoresREF[0] = 0;

	if (R1 != -1){
		_valoresREF[1] = ( R1/(R0+R1) )*1023;
	}
	else _valoresREF[1] = -100; 

	if (R2 != -1){ 
		_valoresREF[2] = ( (R1+R2)/(R0+R1+R2) )*1023;
	}
	else _valoresREF[2] = -100; 

	if (R3 != -1){ 
		_valoresREF[3] = ( (R1+R2+R3)/(R0+R1+R2+R3) )*1023;
	}
	else _valoresREF[3] = -100; 

	if (R4 != -1){ 
		_valoresREF[4] = ( (R1+R2+R3+R4)/(R0+R1+R2+R3+R4) )*1023;
	}
	else _valoresREF[4] = -100; 

	if (R5 != -1){
		_valoresREF[5] = ( (R1+R2+R3+R4+R5)/(R0+R1+R2+R3+R4+R5) )*1023;
	}
	else _valoresREF[5] = -100; 

	/*
	Serial.println(_valoresREF[0]);
	Serial.println(_valoresREF[1]);
	Serial.println(_valoresREF[2]);
	Serial.println(_valoresREF[3]);
	Serial.println(_valoresREF[4]);
	Serial.println(_valoresREF[5]);
	*/
}

int TecladoPAnalog::ler(){

	_leitura = analogRead(_pin);
	for(_i = 0; _i < 6; _i++){
		if (_leitura >= (_valoresREF[_i] - _erro ) && _leitura < (_valoresREF[_i] + _erro))  _leit[_i]=1;
		else  _leit[_i]=0;
	}

	for(_i = 0; _i < 6; _i++){
			  if (_leit[_i] != _ant[_i]) _time[_i] = millis();
			  if ((millis() - _time[_i]) > _debounceDelay) {
				if (_leit[_i] != _estado[_i]) {
				  _estado[_i] = _leit[_i];
				  if (_estado[_i] == 1) return _i;
				}
			  }
			  _ant[_i] = _leit[_i];
	}

	return -1;
}