#include "TecladoPAnalog.h"


TecladoPAnalog teclado(0); //teclado no pino 0


void setup() {
  Serial.begin(9600);
  teclado.resistores(2200, 360, 680, 1000, NO_RES, NO_RES);
  
}

void loop() {
  // put your main code here, to run repeatedly:
  int botao= teclado.ler();
  if (botao != -1) Serial.println(botao);
}
