/*
LEITURA DE TECLADO UTILIZANDO UMA S� PORTA ANAL�GICA

MONITORAMENTO DOS BOTOES


 Resistencias: 2k2, 360R, 680R, 1K (DICA: 2K2 FIXO, AUMENTAR R1,R2,R3.. DE 300 EM 300 ohms
Teste Leituras A0:
B0 - 8
B1 - 142
B2 - 327
B3 - 532
nenhum - 1022

//CIRCUITO


VCC
O---------
		 |
		[ ] R0
		 |			B0
		 |---------[O]----------
		 |					   |
		[ ] R1				   |
		 |			B1		   |		
		 |---------[O]----------
		 |					   |
		[ ] R2			       |
		 |			B2		   |
		 |---------[O]----------
		 |					   |
		[ ] R3			       |
		 |			B3		   |
		 |---------[O]----------
		 |					   |
		[ ] R4			       |
		 |			B4		   |
		 |---------[O]----------
		 |					   |
		[ ] R5			       |
		 |			B5		   |
		 |---------[O]----------
		 					   |
							   |
						       |
							   O  GND

*/



#ifndef TecladoPAnalog_h
#define TecladoPAnalog_h

#include "Arduino.h"


class TecladoPAnalog
{
  public:
    TecladoPAnalog(int pin); //PINO ANALOGICO
	int ler(); //retorna 0, 1, 2, 3, 4... conforme bot�o. -1 se nenhum pressionado
	void resistores(float R0,float R1,float R2,float R3,float R4,float R5);
	#define NO_RES -1

  private:
	float _valoresREF[6];
	int _pin;
	#define _erro 40
	#define _debounceDelay 30 //milis


	bool _ant[6], _estado[6], _leit[6];
	unsigned long _time[6];
	int _i, _leitura;

};

#endif